Feature: user
  As a Taiga user
  I can connect to Taiga metrics

  Background: Set target server address and headers, reset DB
    Given I am using server "$SERVER"
    And I set "Accept" header to "application/json"
    And I set "Content-Type" header to "application/json"
    And I set template variable "EMAIL" to "$EMAIL"
    And I set template variable "PASSWORD" to "$PASSWORD"
    And I set template variable "USER_ID" to "$USER_ID"
    And I set template variable "AUTH_TOKEN" to "$AUTH_TOKEN"
    #Then I reset the database content

  @user
  Scenario: Test Login With Wrong Password should fail
    When I make a POST request to "/sessions"
    """
    {
      "email": "{{EMAIL}}",
      "password": "BAD_PASSWORD"
    }
    """
    Then the response status should be 401

  @user
  Scenario: Test Login With Wrong Username should fail
    When I make a POST request to "/sessions"
    """
    {
      "email": "BAD_USER",
      "password": "{{PASSWORD}}"
    }
    """
    Then the response status should be 401

  @user
  Scenario: User can authenticate using its Taiga credentials
    When I make a POST request to "/sessions"
    """
    {
      "email": "{{EMAIL}}",
      "password": "{{PASSWORD}}"
    }
    """
    Then the response status should be 201
    And the JSON should contain
    """
    {
      "id": {{USER_ID}},
      "email": "{{EMAIL}}",
      "full_name_display": "TEST USER",
      "auth_token": "{{AUTH_TOKEN}}"
    }
    """
    Then I set the header "Authorization" to the JSON value at path "auth_token"
    And I disconnect myself
