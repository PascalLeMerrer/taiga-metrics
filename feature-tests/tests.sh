#!/bin/bash

# This scripts waits for the API server to be ready before running functional tests with Behave
# when invoked with parameters, they are used as tagged to be passed to behave
# It allows running tests with specific tags

until curl --location --silent "http://${TAIGA_METRICS_SERVER}:7000/isalive"; do
  >&2 echo "server is not ready - sleeping"
  sleep 1
done

>&2 echo "\nserver is up - starting tests"

tags="--tags=-wip"

behave $tags --no-skipped --quiet
#--no-color
