import os
from behave_http.environment import before_scenario


server = os.getenv('TAIGA_METRICS_SERVER', 'server')

default_env = {
    'SERVER': f'http://{server}:7000',
    'AUTH_TOKEN': 'eyJ1c2VyX2F1dGhlbnRpY2F0aW9uX2lkIjoxNn0:1dQERV:gMlrZm2vzmC6lRtSqOx0HTUyGGU',
    'EMAIL': 'user@test.com',
    'PASSWORD': 'pass',
    'USER_ID': '16',
}

def before_all(context):
    for k, v in default_env.items():
        os.environ[k] = v

