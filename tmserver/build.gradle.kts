import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.gradle.jvm.tasks.Jar

group = "com.taigametrics"
version = "1.0-SNAPSHOT"
//
//object versions { // TODO fix and use, or remove
//    val kotlin = "1.2.51"
//}

plugins {
    application
    id("org.jetbrains.kotlin.jvm") version "1.2.51"
    id("com.github.johnrengelman.shadow") version "2.0.4"
}

repositories {
    mavenCentral()
    maven(url = "https://dl.bintray.com/kotlin/exposed")
    maven (url = "https://jitpack.io" )
    jcenter()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    implementation(kotlin("stdlib", "1.2.51"))

    compile("io.javalin:javalin:1.7.0")
    compile("io.github.microutils:kotlin-logging:1.5.4")
    compile("org.slf4j:slf4j-simple:1.7.25")

    compile("org.jetbrains.exposed:exposed:0.10.3")

    // Unirest and its dependencies
    compile("com.mashape.unirest:unirest-java:1.4.9")
    compile("org.apache.httpcomponents:httpclient:4.3.6")
    compile("org.apache.httpcomponents:httpasyncclient:4.0.2")
    compile("org.apache.httpcomponents:httpmime:4.3.6")
    compile("org.json:json:20140107")

    compile("com.fasterxml.jackson.core:jackson-core:2.9.6")
    compile("com.fasterxml.jackson.core:jackson-databind:2.9.6")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.6")

    compile("org.postgresql:postgresql:42.2.4")

    testImplementation("junit:junit:4.12")
    testCompile ("io.kotlintest:kotlintest-runner-junit5:3.1.7")
}

application {
    // TODO check the interest
    mainClassName = "com.taigametrics.MainKt"
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}