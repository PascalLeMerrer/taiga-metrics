package com.taigametrics.project

import com.taigametrics.data.Status
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.StringSpec

val authToken = "eyJ1c2VyX2F1dGhlbnRpY2F0aW9uX2lkIjoxNn0:1dQERV:gMlrZm2vzmC6lRtSqOx0HTUyGGU"

class ProjectServiceTest : StringSpec({

    "list should return a list of projects for the authenticated user" {
        val service = ProjectService()
        val projects = service.list(authToken)
        projects.size shouldBe 3

        projects[0].id shouldBe 1
        projects[1].id shouldBe 2
        projects[2].id shouldBe 3

        projects[0].name shouldBe "project1"
        projects[1].name shouldBe "project2"
        projects[2].name shouldBe "project3"

        projects.all { it.userId == 16 } shouldBe true
    }

    "getStatuses should provide user story statuses of project, coming from Taiga" {
        val service = ProjectService()
        val projectId = 2

        val statuses = service.getStatuses(authToken, projectId)

        statuses shouldNotBe null
        statuses.size shouldBe 4
        statuses.get(0) shouldBe Status(id = 1, name = "specifiy", order = 1)
        statuses.get(1) shouldBe Status(id = 2, name = "build", order = 2)
        statuses.get(2) shouldBe Status(id = 3, name = "deploy", order = 3)
        statuses.get(3) shouldBe Status(id = 4, name = "done", order = 4)
    }

    "get should provide details of project, coming from Taiga" {
        val service = ProjectService()
        val projectId = 2

        val project = service.get(authToken, projectId)

        project shouldNotBe null

        project?.id shouldBe projectId
        project?.name shouldBe "project2"
        project?.userId shouldBe 16

        project?.statuses shouldNotBe null
        project?.statuses?.size shouldBe 4
        project?.statuses?.get(0) shouldBe Status(id = 1, name = "specifiy", order = 1)
        project?.statuses?.get(1) shouldBe Status(id = 2, name = "build", order = 2)
        project?.statuses?.get(2) shouldBe Status(id = 3, name = "deploy", order = 3)
        project?.statuses?.get(3) shouldBe Status(id = 4, name = "done", order = 4)
    }

})


