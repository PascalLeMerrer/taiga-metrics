package com.taigametrics.user

import com.taigametrics.data.Credentials
import com.taigametrics.data.User
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.StringSpec

val authToken = "eyJ1c2VyX2F1dGhlbnRpY2F0aW9uX2lkIjoxNn0:1dQERV:gMlrZm2vzmC6lRtSqOx0HTUyGGU"
val email = "user@test.com"
val password="pass"

class UserServiceTest : StringSpec({

    "User service should return User profile when authentication succeeds" {
        val service = UserService()
        val credentials = Credentials(email = email, password = password)
        val user: User? = service.delegateAuthentication(credentials)
        user shouldNotBe null
        user?.id shouldBe 16
        user?.authToken shouldBe authToken
        user?.email shouldBe email
        user?.fullDisplayName shouldBe "TEST USER"
    }

    "User service should return null when authentication fails" {
        val service = UserService()
        val credentials = Credentials(email = email, password = "wrong password")
        val user: User? = service.delegateAuthentication(credentials)
        user shouldBe null
    }

})


