package com.taigametrics.persistence

import mu.KLoggable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.system.exitProcess

class DB {

    companion object : KLoggable {

        override val logger = DB.logger()

        private var connection: Database? = null
        val DB_URL: String
        val DB_USER: String
        val DB_PASSWORD: String
        val UNIX_ERROR = 1

        init {
            val DB_HOST = System.getenv("POSTGRES_HOST") ?: "db"
            val DB_PORT = System.getenv("POSTGRES_PORT") ?: "5432"
            val DB_NAME = System.getenv("POSTGRES_DB") ?: "taigametrics"

            DB_USER = System.getenv("POSTGRES_USER") ?: "taigametrics"

            val password = System.getenv("POSTGRES_PASSWORD")
            if (password == null) {
                DB.logger.error("POSTGRES_PASSWORD environment variable is not set")
                exitProcess(UNIX_ERROR)
            }

            DB_PASSWORD = password
            DB_URL = "jdbc:postgresql://$DB_HOST:$DB_PORT/$DB_NAME"
        }

        // connect is idempotent, and establishes a connection to the persistence
        fun connect() {

            if (!isConnected()) {
                try {
                    connection = Database.connect(DB_URL,
                            driver = "org.postgresql.Driver",
                            user = DB_USER,
                            password = DB_PASSWORD
                    )
                } catch (exception: Exception) {
                    DB.logger.error("DB_URL=$DB_URL, DB_USER=$DB_USER, DB_PASSWORD=$DB_PASSWORD, ")
                    DB.logger.error("Connection to persistence failed with the following error message: ${exception.message}")
                }
            }
        }

        fun isConnected(): Boolean {
            try {
                return connection!!.url.isNotEmpty()
            } catch (exception: Exception) {
                return false
            }
        }

        fun ensureTableExists(table: Table) {
            try {
                transaction {
                    SchemaUtils.create(table)
                }
            } catch (exception: Exception) {
                DB.logger.error("Database error: ${exception.message}")
            }
        }
    }

}

