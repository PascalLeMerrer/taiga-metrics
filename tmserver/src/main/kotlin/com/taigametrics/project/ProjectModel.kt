package com.taigametrics.project

import project.ProjectPersistence
import com.taigametrics.data.Project

class ProjectModel {
    val persistence = ProjectPersistence()
    val service = ProjectService()

    fun getAll(authToken: String): List<Project> {
        return service.list(authToken)
    }

    fun get(authToken: String, projectId: Int): Project? {
        return service.get(authToken, projectId)
    }
}