package project

import com.taigametrics.persistence.DB
import com.taigametrics.persistence.DB.Companion.logger
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.transactions.transaction
import com.taigametrics.data.Project

class ProjectPersistence {

    object Projects : IntIdTable() {
        var user_id = integer("user_id")
        var name = varchar("name", 50) // TODO check max length of project name
        var work_start_status = integer("work_start_status")
        var work_end_status = integer("work_end_status ")
    }

    class Project(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<Project>(Projects)
        var userId by Projects.user_id
        var name by Projects.name
        var work_start_status by Projects.work_start_status
        var work_end_status by Projects.work_end_status
    }

    init {
        DB.connect()
        DB.ensureTableExists(Projects)
    }

    fun getProjects(userId: Int): List<com.taigametrics.data.Project> {
        var projects = mutableListOf<com.taigametrics.data.Project>()

        try {
            transaction {
                val rows = Project.find({
                    Projects.user_id eq userId
                })
                projects = createProjecList(rows)
            }

        } catch (exception: Exception) {
            logger.error("Database error when getting projects for user $userId: ${exception.message}")
        }
        return projects
    }

    private fun createProjecList(rows:SizedIterable<ProjectPersistence.Project>): MutableList<com.taigametrics.data.Project> {
        val projects = mutableListOf<com.taigametrics.data.Project>()
        rows.forEach {
            projects.add(Project(
                    id = it.id.value,
                    userId = it.userId,
                    name = it.name,
                    workStartStatus = it.work_start_status,
                    workEndStatus = it.work_end_status
            ))
        }
        return projects
    }
}
