package com.taigametrics.project

import io.javalin.Context

class ProjectController {

    val model = ProjectModel()

    fun getAll(ctx: Context) {
        val authToken = ctx.attribute("authToken") as String
        val projectList = model.getAll(authToken)
        // TODO gérer les cas d'erreur
        ctx.json(projectList).resultString()
        ctx.status(200)
    }

    fun get(ctx: Context) {
        val authToken = ctx.attribute("authToken") as String
        val projectIdAsString = ctx.param("projectId")
        val projectId = projectIdAsString?.toInt()
        if (projectId == null) {
            ctx.json("{ \"error\": \"Invalid project ID in URL\"}").resultString()
            ctx.status(400)
            return
        }
        val project = model.get(authToken, projectId )
        // TODO gérer les cas d'erreur
        if (project == null) {
            ctx.status(401)
        } else {
            ctx.json(project).resultString()
            ctx.status(200)
        }
    }
}