package com.taigametrics.project

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.Unirest
import com.taigametrics.data.Project
import com.taigametrics.data.Status

class ProjectService() {

    val API_URL = System.getenv("TAIGA_API_URL") ?: "https://api.taiga.io/api/v1"
    val mapper = jacksonObjectMapper()

    fun list(authToken: String): List<Project> {

        val jsonResponse: HttpResponse<String> = Unirest.get("$API_URL/projects")
                .header("Content-Type", "application/json")
                .header("Authorization:", "Bearer ${authToken}")
                .asString()

        return when (jsonResponse.status) {
            200 -> mapper.readValue(jsonResponse.body)
            401 -> emptyList()
            else -> {
                println("${jsonResponse.status} ${jsonResponse.statusText} ${jsonResponse.body}") // TODO log
                throw Exception("Cannot get project list due to a technical issue.")
            }
        }
    }

    fun get(authToken: String, projectId: Int): Project? {
        val jsonResponse: HttpResponse<String> = Unirest.get("$API_URL/projects/$projectId")
                .header("Content-Type", "application/json")
                .header("Authorization:", "Bearer ${authToken}")
                .asString()

        return when (jsonResponse.status) {
            200 -> mapper.readValue(jsonResponse.body)
            401 -> null
            else -> {
                println("${jsonResponse.status} ${jsonResponse.statusText} ${jsonResponse.body}") // TODO log
                throw Exception("Cannot get project due to a technical issue.")
            }
        }
    }

    // TODO SEE IF THIS CODE IS USEFUL (and the answer is: probably not)
    fun getStatuses(authToken: String, projectId: Int): List<Status> {
        val jsonResponse: HttpResponse<String> = Unirest.get("$API_URL/userstory-statuses?project=$projectId")
                .header("Content-Type", "application/json")
                .header("Authorization:", "Bearer ${authToken}")
                .asString()

        return when (jsonResponse.status) {
            200 -> mapper.readValue(jsonResponse.body)
            401 -> emptyList()
            else -> {
                println("${jsonResponse.status} ${jsonResponse.statusText} ${jsonResponse.body}") // TODO log
                throw Exception("Cannot get project statuses due to a technical issue.")
            }
        }
    }


}