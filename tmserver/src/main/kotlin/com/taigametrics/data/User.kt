package com.taigametrics.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class User(

        val id: Int,

        @JsonProperty("auth_token")
        val authToken: String = "",

        val email: String,

        @JsonProperty("full_name_display")
        val fullDisplayName: String
)