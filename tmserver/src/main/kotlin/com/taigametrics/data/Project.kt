package com.taigametrics.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Project (
        val id:Int,

        @JsonProperty("user_id")
        var userId: Int,

        val name: String,

        @JsonProperty("work_start_status_id")
        val workStartStatus: Int? =null,

        @JsonProperty("work_end_status_id")
        val workEndStatus: Int? = null) {

        @JsonProperty("owner")
        private fun unpackNested(owner: Map<String, Any>) {
                this.userId = owner["id"] as Int
        }

        @JsonProperty("us_statuses")
        val statuses:List<Status>? = null
}