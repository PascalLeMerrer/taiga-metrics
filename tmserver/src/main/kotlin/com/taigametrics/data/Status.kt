package com.taigametrics.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Status(
        val id: Int,
        val name: String,
        val order: Int)