package com.taigametrics.data

data class Credentials(val email: String?, val password: String?) {
    fun isIncomplete():Boolean {
        return this.email.isNullOrBlank()|| this.password.isNullOrBlank()
    }
}