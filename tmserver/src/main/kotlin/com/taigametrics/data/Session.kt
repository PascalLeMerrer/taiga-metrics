package com.taigametrics.data

import org.joda.time.DateTime

data class Session (val id: Int, val userId: Int, val authToken: String, val expirationDate: DateTime)
