package com.taigametrics

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.mashape.unirest.http.ObjectMapper
import com.mashape.unirest.http.Unirest
import com.taigametrics.project.ProjectController
import com.taigametrics.user.TaigaMetricsRole
import com.taigametrics.user.UserController
import io.javalin.ApiBuilder.*
import io.javalin.Javalin
import io.javalin.embeddedserver.Location
import io.javalin.security.Role.roles
import io.javalin.translator.json.JavalinJacksonPlugin
import java.io.IOException

val indexPage = """
<!DOCTYPE html>
<html>

<head>
    <title>Taiga Metrics</title>
    <link rel="stylesheet" type="text/css" href="/css/bulma.css">
    <script type="text/javascript" src="/js/taigametrics.js"></script>
</head>

<body>
    <script type="text/javascript">
        Elm.TaigaMetrics.init();
    </script>
</body>

</html>
"""


fun main(args: Array<String>) {
    val port = 7000

    val app = Javalin.create().apply {
        disableStartupBanner() // remove the javalin startup banner from logs
        enableCorsForOrigin("*") // enables cors for the specified origin(s)
        enableRouteOverview("/path") // render a HTML page showing all mapped routes
        enableDynamicGzip() // gzip response (if client accepts gzip and response is more than 1500 bytes)
        enableStaticFiles("src/main/resources/public", Location.EXTERNAL)
        port(port)
        error(404) { ctx -> ctx.redirect("/index.html") }
        exception(Exception::class.java) { e, _ -> e.printStackTrace() }
//    }.requestLogLevel(LogLevel.EXTENSIVE).start()
    }.start()

    initObjectMapper()

    val userController = UserController()
    val projectController = ProjectController()


    app.accessManager { handler, ctx, permittedRoles ->
        userController.preventUnauthorizedAccess(ctx, permittedRoles, handler)
    }

    app.routes {
        get("/", { ctx -> ctx.html(indexPage) }, roles(TaigaMetricsRole.ANYONE))
        get("/isalive", { ctx -> ctx.result("{ \"status\": \"OK\" }") }, roles(TaigaMetricsRole.ANYONE))

        post("/sessions", { ctx -> userController.authenticate(ctx) }, roles(TaigaMetricsRole.ANYONE))
        delete("/sessions", { ctx -> userController.logout(ctx) }, roles(TaigaMetricsRole.AUTHENTICATED))

        get("/projects/:projectId", { ctx -> projectController.get(ctx) }, roles(TaigaMetricsRole.AUTHENTICATED))
        get("/projects", { ctx -> projectController.getAll(ctx) }, roles(TaigaMetricsRole.AUTHENTICATED))
    }
}

fun initObjectMapper() {

    val mapper = jacksonObjectMapper()
    mapper.enable(SerializationFeature.INDENT_OUTPUT)
    mapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS)
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    JavalinJacksonPlugin.configure(mapper)

    Unirest.setObjectMapper(object : ObjectMapper {
        private val jacksonObjectMapper = com.fasterxml.jackson.databind.ObjectMapper()

        init {
            jacksonObjectMapper.enable(SerializationFeature.INDENT_OUTPUT)
            jacksonObjectMapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS)
        }

        override fun <T> readValue(value: String, valueType: Class<T>): T {
            try {
                println("deserialising $value")
                return jacksonObjectMapper.readValue(value, valueType)
            } catch (e: IOException) {
                println(e.message)
                throw RuntimeException(e)
            }

        }

        override fun writeValue(value: Any): String {
            try {
                return jacksonObjectMapper.writeValueAsString(value)
            } catch (e: JsonProcessingException) {
                println(e.message)
                throw RuntimeException(e)
            }

        }
    })
}
