package com.taigametrics.user

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.Unirest
import com.taigametrics.data.Credentials
import com.taigametrics.data.User

class UserService() {

    val API_URL = System.getenv("TAIGA_API_URL") ?: "https://api.taiga.io/api/v1"
    val mapper = jacksonObjectMapper()

    // makes a request to Taiga to authenticate the user with the provided credentials
    fun delegateAuthentication(credentials: Credentials): User? {

        val requestBody = """
            {
                "type": "normal",
                "username": "${credentials.email}",
                "password": "${credentials.password}"
            }
            """.trimIndent()

        val jsonResponse: HttpResponse<String> = Unirest.post("$API_URL/auth")
                .header("Content-Type", "application/json")
                .body(requestBody)
                .asString()

        return when (jsonResponse.status) {
            200, 201 -> mapper.readValue(jsonResponse.body)
            401 -> null
            else -> {
                println(jsonResponse.body)
                throw Exception("Authentication failed due to a technical issue.")
            }
        }
    }
}