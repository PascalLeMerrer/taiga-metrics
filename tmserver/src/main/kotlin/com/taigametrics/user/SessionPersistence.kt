package com.taigametrics.user

import com.taigametrics.persistence.DB
import com.taigametrics.data.Session
import com.taigametrics.data.User
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime


// TODO rewrite using Exposed DSL instead of DAO; the DAO forces either to create many objects,
// or to spread DB related concept into the model
class SessionPersistence {

    val SESSION_DURATION_IN_DAYS = 1

    object Sessions : IntIdTable() {
        val userId = integer("user_id")
        val authToken = varchar("auth_token", 80).primaryKey()
        val expirationDate = datetime("expiration_date")
    }

    class Session(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<Session>(Sessions)

        var userId by Sessions.userId
        var authToken by Sessions.authToken
        var expirationDate by Sessions.expirationDate
    }

    init {
        DB.connect()
        DB.ensureTableExists(Sessions)
    }

    fun createOrUpdateSession(user: User) {
        transaction {
            val sessionVO = getSession(user.authToken)
            if (sessionVO != null) {
                updateSession(sessionVO)
            } else {
                val sessionDAO = Session.new {
                    userId = user.id
                    authToken = user.authToken
                    expirationDate = DateTime.now().plusDays(SESSION_DURATION_IN_DAYS)
                }
                sessionDAO.flush()
            }
        }
    }

    fun getSession(token: String): com.taigametrics.data.Session? {
        var session: com.taigametrics.data.Session? = null
        transaction {
            val sessionDAOs = Session.find { Sessions.authToken eq token }

            sessionDAOs.forEach {
                session = Session(it.id.value, it.userId, it.authToken, it.expirationDate)
            }
        }
        return session
    }

    fun updateSession(session: com.taigametrics.data.Session) {
        transaction {
            val sessionDAO = Session.findById(session.id)
            sessionDAO?.expirationDate = DateTime.now().plusDays(SESSION_DURATION_IN_DAYS)
        }
    }

    fun deleteSession(token: String) {
        transaction {
            val sessionDAOs = Session.find { Sessions.authToken eq token }
            sessionDAOs.forEach {
                it.delete()
            }
        }
    }
}