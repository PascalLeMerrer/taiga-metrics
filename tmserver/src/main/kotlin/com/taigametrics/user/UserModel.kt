package com.taigametrics.user

import com.taigametrics.data.Credentials
import com.taigametrics.data.User

class UserModel() {

    private val userService = UserService()
    private val sessionService = SessionPersistence()

    fun authenticate(credentials: Credentials): User? {
        val userVO = userService.delegateAuthentication(credentials)
        if (userVO != null) {
            sessionService.createOrUpdateSession(userVO)
        }
        return userVO
    }

    fun endSession(token: String) {
        sessionService.deleteSession(token)
    }

    fun getAuthenticatedUserId(token: String): Int? {
        val session = sessionService.getSession(token)
        if (session != null)  {
            sessionService.updateSession(session)
            return session.userId
        }
        return null

    }
}
