package com.taigametrics.user

import com.taigametrics.data.Credentials
import io.javalin.Context
import io.javalin.Handler
import io.javalin.security.Role


enum class TaigaMetricsRole : Role {
    ANYONE, AUTHENTICATED, ADMIN
}

class UserController {

    companion object {
        val AUTHORIZATION_HEADER = "Authorization"
    }

    val model = UserModel()

    fun authenticate(ctx: Context) {
        val credentials = ctx.bodyAsClass(Credentials::class.java)
        if (credentials.isIncomplete()) {
            ctx.result("{ 'error': 'incomplete credentials' }")
            ctx.status(401)
            return
        }
        try {
            val user = model.authenticate(credentials)
            if (user == null) {
                ctx.result("{ 'error': 'invalid credentials' }")
                ctx.status(401)
            } else {
                ctx.json(user).resultString()
                ctx.status(201)
            }
        } catch (exception: Exception) {
            ctx.result("{ 'error': '${exception.message}' }")
            ctx.status(500)
        }
    }

    fun logout(ctx: Context) {
        val token = ctx.header(AUTHORIZATION_HEADER)
        if (token != null) {
            model.endSession(token)
        }
        ctx.status(204)
    }

    fun preventUnauthorizedAccess(ctx: Context, permittedRoles: MutableList<Role>, handler: Handler) {
        val userRole = getUserRole(ctx) // determine user role based on request
        if (permittedRoles.contains(userRole)) {
            handler.handle(ctx)
        } else {
            ctx.status(401).result("Unauthorized")
        }
    }

    fun getUserRole(ctx: Context): Role {
        val authHeader = ctx.header(UserController.AUTHORIZATION_HEADER)
        if (authHeader == null)
        {
            return TaigaMetricsRole.ANYONE
        }

        val userId = model.getAuthenticatedUserId(authHeader)
        if (userId == null) {
            return TaigaMetricsRole.ANYONE
        }
        ctx.attribute("userId", userId)
        ctx.attribute("authToken", authHeader)
        return TaigaMetricsRole.AUTHENTICATED
    }
}


