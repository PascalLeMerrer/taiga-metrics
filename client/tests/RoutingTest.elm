module RoutingTest exposing (testLocation, testRoutes)

import Debug
import Expect
import Routing exposing (..)
import Test exposing (..)
import Url exposing (Protocol(..), Url)


testRoutes : Test
testRoutes =
    describe "Route.fromLocation"
        [ testLocation "" HomeRoute
        , testLocation "/projects" ProjectsRoute
        , testLocation "/projects/1" (ProjectRoute 1)
        , testLocation "/some_random_string" NotFoundRoute
        ]


testLocation : String -> Route -> Test
testLocation path route =
    test ("Path: \"" ++ path ++ "\" should return the expected route") <|
        \() ->
            createTestUrl path
                |> Routing.toRoute
                |> Expect.equal route


createTestUrl path =
    { protocol = Http
    , host = "domainname"
    , port_ = Nothing
    , path = path
    , query = Nothing
    , fragment = Nothing
    }
