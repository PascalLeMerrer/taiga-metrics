module ProjectListUpdatesTest exposing (testEmptyProjectListUpdate, testProjectListUpdate)

import Expect
import Fixtures
import Pages.ProjectList exposing (ProjectListMessage(..), updateProjectList)
import RemoteData exposing (RemoteData)
import Test exposing (Test, describe, test)



-- I'm not convinced by the interest of these tests,
-- as the tested code is so simple I don't see how it could be wrong,
-- but I keep them as an example of update function testing


testEmptyProjectListUpdate : Test
testEmptyProjectListUpdate =
    test "Server returning an empty project list should be reflected into model" <|
        \() ->
            let
                apiResponse =
                    HandleProjectListResponse (RemoteData.Success [])

                ( subModel, notification, subMsg ) =
                    updateProjectList apiResponse "a token" Pages.ProjectList.init
            in
            Expect.equal [] subModel


testProjectListUpdate : Test
testProjectListUpdate =
    test "Server returning a project list should be reflected into model" <|
        \() ->
            let
                model =
                    Fixtures.projectList

                apiResponse =
                    HandleProjectListResponse (RemoteData.Success model)

                ( subModel, notification, subMsg ) =
                    updateProjectList apiResponse "a token" Pages.ProjectList.init
            in
            Expect.equal
                [ Fixtures.project1
                , Fixtures.project2
                , Fixtures.project3
                ]
                subModel
