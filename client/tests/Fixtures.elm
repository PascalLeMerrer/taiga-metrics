module Fixtures exposing (appModel, loginForm, project1, project2, project3, projectList, user)

import Browser.Navigation as Nav exposing (Key)
import Data.Project exposing (Project)
import Data.User exposing (User)
import Model exposing (..)
import Pages.LoginForm
import Pages.Project
import Pages.ProjectList
import RemoteData
import Routing exposing (Route(..))
import Url exposing (Protocol(..), Url)
import Views.Notifications exposing (emptyNotification)


homeUrl : Url
homeUrl =
    { protocol = Http
    , host = "domainname"
    , port_ = Nothing
    , path = "somepath"
    , query = Nothing
    , fragment = Nothing
    }


appModel : AppModel
appModel =
    { loginForm = loginForm
    , notification = emptyNotification
    , project = Pages.Project.init
    , projectList = Pages.ProjectList.init
    , route = ProjectsRoute
    , homeUrl = homeUrl
    , url = homeUrl
    , key = Nothing
    }


loginForm : Pages.LoginForm.Model
loginForm =
    { authenticated = True
    , isPasswordVisible = False
    , password = "pass"
    , token = user.auth_token
    , userStatus = RemoteData.Success user
    , email = "email@test.com"
    }


user : User
user =
    { auth_token = "AUTHENTICATION TOKEN"
    , full_display_name = "Full display name"
    , username = "user name"
    }


project1 : Project
project1 =
    { name = "My project #1"
    , id = 42
    , userId = 16
    , statuses =
        [ { id = 1, name = "specifiy", order = 1 }
        , { id = 2, name = "build", order = 2 }
        , { id = 3, name = "deploy", order = 3 }
        , { id = 4, name = "done", order = 4 }
        ]
    }


project2 : Project
project2 =
    { name = "My project #2"
    , id = 43
    , userId = 16
    , statuses =
        [ { id = 1, name = "specifiy", order = 1 }
        , { id = 2, name = "build", order = 2 }
        , { id = 3, name = "deploy", order = 3 }
        , { id = 4, name = "done", order = 4 }
        ]
    }


project3 : Project
project3 =
    { name = "My project #3"
    , id = 44
    , userId = 16
    , statuses =
        [ { id = 1, name = "specifiy", order = 1 }
        , { id = 2, name = "build", order = 2 }
        , { id = 3, name = "deploy", order = 3 }
        , { id = 4, name = "done", order = 4 }
        ]
    }


projectList : Pages.ProjectList.Model
projectList =
    [ project1
    , project2
    , project3
    ]
