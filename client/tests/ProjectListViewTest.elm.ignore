module ProjectListViewTest exposing (testProjectLink, testProjectLinks, testProjectListView)

-- waiting for Test.Html.Query and Test.Html.Query pacakges to be updated for ELM0.19

import ElmTest.Extra exposing (Test, describe, test)
import Expect exposing (Expectation)
import Fixtures exposing (appModel)
import Html.Attributes as Attr
import Pages.ProjectList.View exposing (viewProjectList)
import Test.Html.Query as Query
import Test.Html.Selector exposing (attribute, tag, text)


testProjectListView : Test
testProjectListView =
    test "Project view should contains a link for each project of the model" <|
        \_ ->
            { appModel | projectList = Fixtures.projectList }
                |> viewProjectList
                |> Query.fromHtml
                |> Query.findAll [ tag "a" ]
                |> Query.count (Expect.equal 3)


testProjectLinks : Test
testProjectLinks =
    describe "Projects links should target detailed views and display project names"
        [ testProjectLink 0 "#projects/42" "My project #1"
        , testProjectLink 1 "#projects/43" "My project #2"
        , testProjectLink 2 "#projects/44" "My project #3"
        ]


testProjectLink : Int -> String -> String -> Test
testProjectLink index link name =
    test "Project link should target detailed project view and display project name" <|
        \_ ->
            { appModel | projectList = Fixtures.projectList }
                |> viewProjectList
                |> Query.fromHtml
                |> Query.findAll [ tag "a" ]
                |> Query.index index
                |> Query.has
                    [ attribute <| Attr.href link
                    , text name
                    ]
