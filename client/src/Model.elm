module Model exposing (AppModel, Msg(..), init, reset)

import Browser
import Browser.Navigation as Nav
import Pages.LoginForm
import Pages.Project
import Pages.ProjectList
import Routing exposing (..)
import Url exposing (Url)
import Views.Notifications exposing (..)


type Msg
    = CloseNotification
    | ConnectionMsg Pages.LoginForm.Msg
    | LinkClicked Browser.UrlRequest
    | Logout
    | ProjectListMsg Pages.ProjectList.ProjectListMessage
    | ProjectMsg Pages.Project.ProjectMessage
    | UrlChanged Url.Url


type alias AppModel =
    { homeUrl : Url
    , key : Maybe Nav.Key

    -- TODO replace login`form with login?
    , loginForm : Pages.LoginForm.Model
    , notification : Notification
    , project : Pages.Project.Model
    , projectList : Pages.ProjectList.Model
    , route : Route
    , url : Url.Url
    }


init : () -> Url.Url -> Nav.Key -> ( AppModel, Cmd Msg )
init flags url key =
    ( { key = Just key
      , homeUrl = url
      , loginForm = Pages.LoginForm.init
      , project = Pages.Project.init
      , projectList = Pages.ProjectList.init
      , notification = emptyNotification
      , route = toRoute url
      , url = url
      }
    , Cmd.none
    )


reset : AppModel -> AppModel
reset model =
    let
        urlModel =
            model.url
    in
    { homeUrl = model.homeUrl
    , key = model.key
    , loginForm = Pages.LoginForm.init
    , project = Pages.Project.init
    , projectList = Pages.ProjectList.init
    , notification = emptyNotification
    , route = toRoute model.homeUrl
    , url = { urlModel | path = "/", query = Nothing, fragment = Nothing }
    }
