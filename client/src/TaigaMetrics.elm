module TaigaMetrics exposing (main)

import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Model exposing (..)
import Pages.LoginForm exposing (updateConnectionForm, viewLoginForm)
import Pages.Project exposing (ProjectMessage(..), updateProject, viewProject)
import Pages.ProjectList exposing (ProjectListMessage(..), updateProjectList, viewProjectList)
import RemoteData exposing (RemoteData)
import Requests.ProjectRequests exposing (..)
import Routing exposing (Route(..), pushUrl, toRoute)
import Url
import Views.Notifications exposing (..)
import Views.ViewUtils exposing (classes)



-- MAIN


main : Program () AppModel Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- UPDATE


update : Msg -> AppModel -> ( AppModel, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model
                    , pushUrl model.key (Url.toString url)
                    )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            let
                newModel =
                    { model
                        | url = url
                        , route = toRoute url
                    }
            in
            ( newModel
            , triggerOnLoadAction newModel
            )

        CloseNotification ->
            ( { model | notification = emptyNotification }, Cmd.none )

        ConnectionMsg connectionMsg ->
            let
                ( subModel, notification, subMsg ) =
                    updateConnectionForm connectionMsg model.loginForm model.key
            in
            ( { model | loginForm = subModel, notification = notification }, Cmd.map ConnectionMsg subMsg )

        ProjectListMsg projectListMsg ->
            let
                ( subModel, notification, subMsg ) =
                    updateProjectList projectListMsg model.loginForm.token model.projectList
            in
            ( { model | projectList = subModel, notification = notification }, Cmd.map ProjectListMsg subMsg )

        ProjectMsg projectMsg ->
            let
                ( subModel, notification, subMsg ) =
                    updateProject projectMsg model.project
            in
            ( { model | project = subModel, notification = notification }, Cmd.map ProjectMsg subMsg )

        Logout ->
            ( reset model, Cmd.none )


triggerOnLoadAction : AppModel -> Cmd Msg
triggerOnLoadAction model =
    case model.route of
        ProjectsRoute ->
            listProjects model.loginForm.token HandleProjectListResponse
                |> Cmd.map ProjectListMsg

        ProjectRoute id ->
            getProject model.loginForm.token id HandleProjectResponse
                |> Cmd.map ProjectMsg

        _ ->
            Cmd.none



-- SUBSCRIPTIONS


subscriptions : AppModel -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : AppModel -> Browser.Document Msg
view model =
    { title = "Taiga Metrics"
    , body =
        [ div
            []
            [ viewNavbar model
            , viewColumns <| viewMainContent model
            , viewColumns <| viewNotification model
            ]
        ]
    }


viewMainContent : AppModel -> Html Msg
viewMainContent model =
    case model.route of
        HomeRoute ->
            Html.map ConnectionMsg <| viewLoginForm model.loginForm

        ProjectsRoute ->
            Html.map ProjectListMsg <| viewProjectList model.projectList

        ProjectRoute projectId ->
            Html.map ProjectMsg <| viewProject model.project

        NotFoundRoute ->
            notFoundView


viewNavbar : AppModel -> Html Msg
viewNavbar model =
    nav [ class "navbar is-black", attribute "role" "navigation", attribute "aria-label" "main navigation" ]
        [ div [ class "navbar-brand" ]
            [ a [ href "/" ]
                [ h1 [ class "navbar-item has-text-white" ] [ text "Taiga Metrics" ]
                ]
            ]
        , div [ class "navbar-end" ]
            [ div [ class "navbar-item" ] [ viewConnectedUser model ]
            , div [ class "navbar-item" ] [ viewLogoutButton model.loginForm ]
            ]
        ]


viewConnectedUser : AppModel -> Html Msg
viewConnectedUser model =
    if model.loginForm.authenticated then
        text model.loginForm.email

    else
        text ""


viewColumns : Html Msg -> Html Msg
viewColumns centralColumContent =
    section [ class "section" ]
        [ div [ class "columns" ]
            [ div [ class "column" ] []
            , div [ class "column" ]
                [ centralColumContent ]
            , div [ class "column" ] []
            ]
        ]


viewNotification : AppModel -> Html Msg
viewNotification model =
    let
        messageClass =
            case model.notification.messageType of
                SuccessNotification ->
                    "is-success"

                ErrorNotification ->
                    "is-danger"

                NoNotification ->
                    ""
    in
    if model.notification.messageType == NoNotification then
        span [] []

    else
        article [ classes [ "message", messageClass ] ]
            [ div [ class "message-header" ]
                [ p [] [ text model.notification.title ]
                , button [ class "delete", onClick CloseNotification ] []
                ]
            , div [ class "message-body" ]
                [ text model.notification.body ]
            ]


viewLogoutButton : Pages.LoginForm.Model -> Html Msg
viewLogoutButton connection =
    let
        logoutButtonClass =
            [ "button"
            , if connection.userStatus == RemoteData.Loading then
                "is-loading"

              else
                ""
            ]
    in
    if connection.authenticated then
        div [ classes [ "field has-addons-centered" ] ]
            [ p [ class "control" ]
                [ button
                    [ classes logoutButtonClass
                    , onClick Logout
                    ]
                    [ text "Me déconnecter" ]
                ]
            ]

    else
        text ""


notFoundView : Html msg
notFoundView =
    div []
        [ text "La page demandée n'existe pas."
        ]
