module Requests.ConnectionRequests exposing (connect, deleteConfig, disconnect)

import Data.User exposing (..)
import Http
import Json.Encode exposing (object, string)
import RemoteData exposing (RemoteData(..), WebData)
import RemoteData.Http exposing (Config, deleteWithConfig, post)


connect : String -> String -> String -> (WebData User -> msg) -> Cmd msg
connect email password token message =
    let
        body =
            Json.Encode.object
                [ ( "email", Json.Encode.string email )
                , ( "password", Json.Encode.string password )
                ]
    in
    post "/sessions" message userDecoder body


disconnect : String -> (WebData String -> msg) -> Cmd msg
disconnect token message =
    deleteWithConfig (deleteConfig token) "/sessions" message (string "")


deleteConfig : String -> Config
deleteConfig token =
    { headers =
        [ Http.header "Accept" "application/json"
        , Http.header "Authorization" token
        ]
    , withCredentials = False
    , timeout = Nothing
    }
