module Requests.ProjectRequests exposing (getProject, listProjects)

import Data.Project exposing (Project, projectDecoder)
import Data.ProjectList exposing (projectListDecoder)
import Http
import RemoteData exposing (WebData)
import RemoteData.Http exposing (Config, getWithConfig)


listProjects : String -> (WebData (List Project) -> msg) -> Cmd msg
listProjects token message =
    getWithConfig (config token) "/projects" message projectListDecoder


getProject : String -> Int -> (WebData Project -> msg) -> Cmd msg
getProject token projectId message =
    let
        path =
            "/projects/" ++ String.fromInt projectId
    in
    getWithConfig (config token) path message projectDecoder



-- TODO factoriser pour éviter de dupliquer dans chaque Request.elm


config : String -> Config
config token =
    { headers =
        [ Http.header "Accept" "application/json"
        , Http.header "Authorization" token
        ]
    , withCredentials = False
    , timeout = Nothing
    }
