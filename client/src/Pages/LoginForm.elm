module Pages.LoginForm exposing (Model, Msg, init, updateConnectionForm, viewLoginForm)

import Browser
import Browser.Navigation as Nav
import Data.Connection exposing (Connection)
import Data.User exposing (User)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http exposing (Error(..))
import RemoteData exposing (RemoteData, WebData)
import Requests.ConnectionRequests exposing (connect, disconnect)
import Routing exposing (pushUrl)
import Views.Notifications exposing (..)
import Views.ViewUtils exposing (classes)


type Msg
    = ChangePassword String
    | ChangeUsername String
    | HandleLoginResponse (WebData User)
    | HandleLogoutResponse (WebData String)
    | Login
    | Logout
    | TogglePasswordVisibility Bool


type alias Model =
    Connection


init : Model
init =
    { authenticated = False
    , email = ""
    , isPasswordVisible = False
    , password = ""
    , token = ""
    , userStatus = RemoteData.NotAsked
    }


updateConnectionForm : Msg -> Model -> Maybe Nav.Key -> ( Model, Notification, Cmd Msg )
updateConnectionForm msg model key =
    case msg of
        ChangeUsername login ->
            ( { model | email = login }, emptyNotification, Cmd.none )

        ChangePassword pass ->
            ( { model | password = pass }, emptyNotification, Cmd.none )

        Login ->
            ( { model | userStatus = RemoteData.Loading }
            , emptyNotification
            , connect model.email model.password model.token HandleLoginResponse
            )

        HandleLoginResponse RemoteData.NotAsked ->
            ( { model | userStatus = RemoteData.NotAsked }
            , emptyNotification
            , Cmd.none
            )

        HandleLoginResponse (RemoteData.Failure httpError) ->
            ( { model | userStatus = RemoteData.Failure httpError }
            , authenticationFailedNotification
            , Cmd.none
            )

        HandleLoginResponse (RemoteData.Success user) ->
            let
                updatedModel =
                    { model
                        | authenticated = True
                        , userStatus = RemoteData.Success user
                        , token = user.auth_token
                    }
            in
            ( updatedModel, emptyNotification, pushUrl key "/projects" )

        Logout ->
            ( { model
                | userStatus = RemoteData.Loading
                , password = ""
              }
            , emptyNotification
            , disconnect model.token HandleLogoutResponse
            )

        HandleLogoutResponse (RemoteData.Success _) ->
            ( { model
                | authenticated = False
                , userStatus = RemoteData.NotAsked
              }
            , emptyNotification
            , pushUrl key "/"
            )

        HandleLogoutResponse (RemoteData.Failure httpError) ->
            ( { model
                | userStatus = RemoteData.Failure httpError
              }
            , { deconnectionErrorNotification | body = httpErrorToString httpError }
            , Cmd.none
            )

        TogglePasswordVisibility value ->
            ( { model | isPasswordVisible = value }
            , emptyNotification
            , Cmd.none
            )

        _ ->
            ( model, emptyNotification, Cmd.none )


httpErrorToString : Http.Error -> String
httpErrorToString error =
    case error of
        BadUrl message ->
            message

        Timeout ->
            "Time out"

        NetworkError ->
            "Network error"

        BadStatus response ->
            response.status.message

        BadPayload str response ->
            str ++ " " ++ response.status.message


viewLoginForm : Model -> Html Msg
viewLoginForm model =
    Html.form [ onSubmit <| Login ]
        [ viewInputField <| viewUsernameField
        , viewInputField <| viewPasswordField model
        , viewVisibilityCheckbox model
        , viewLoginButton model
        ]


viewInputField : ( String, List (Html Msg) ) -> Html Msg
viewInputField content =
    div [ class "fied" ]
        [ label [ class "label" ] [ text <| Tuple.first content ]
        , p [ class "control has-icons-left" ] <| Tuple.second content
        ]


viewUsernameField : ( String, List (Html Msg) )
viewUsernameField =
    ( "Adresse e-mail"
    , [ input
            [ class "input"
            , onInput ChangeUsername
            ]
            []
      ]
    )


viewPasswordField : Model -> ( String, List (Html Msg) )
viewPasswordField model =
    ( "Mot de passe"
    , [ input
            [ class "input"
            , type_ <| viewPasswordType model
            , onInput ChangePassword
            , value model.password
            ]
            []
      , span [ class "icon is-small is-left" ]
            [ i [ class "fa fa-lock" ] []
            ]
      ]
    )


viewVisibilityCheckbox : Model -> Html Msg
viewVisibilityCheckbox model =
    div [ class "field" ]
        [ label [ class "checkbox" ]
            []
        , input
            [ checked model.isPasswordVisible
            , type_ "checkbox"
            , onClick <| TogglePasswordVisibility <| not model.isPasswordVisible
            ]
            []
        , text " Afficher le mot de passe"
        ]


viewLoginButton : Model -> Html Msg
viewLoginButton model =
    let
        loginButtonClass =
            [ "button"
            , "is-primary"
            , if model.userStatus == RemoteData.Loading then
                "is-loading"

              else
                ""
            ]
    in
    div [ classes [ "field has-addons-centered" ] ]
        [ p [ class "control" ]
            [ button
                [ classes loginButtonClass
                ]
                [ text "Me connecter" ]
            ]
        ]


viewPasswordType : Model -> String
viewPasswordType model =
    if model.isPasswordVisible then
        "text"

    else
        "password"
