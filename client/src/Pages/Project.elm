module Pages.Project exposing (Model, ProjectMessage(..), init, updateProject, viewProject)

import Data.Project exposing (Project)
import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData exposing (RemoteData, WebData)
import Views.Notifications exposing (..)


type ProjectMessage
    = HandleProjectResponse (WebData Project)


type alias Model =
    { projectId : Int
    , project : Maybe Project
    }


init : Model
init =
    { projectId = 0
    , project = Nothing
    }


updateProject : ProjectMessage -> Model -> ( Model, Notification, Cmd ProjectMessage )
updateProject msg model =
    case msg of
        HandleProjectResponse RemoteData.NotAsked ->
            ( model
            , emptyNotification
            , Cmd.none
            )

        HandleProjectResponse (RemoteData.Failure httpError) ->
            ( model
            , projectFailedNotification
            , Cmd.none
            )

        HandleProjectResponse (RemoteData.Success project) ->
            ( { model
                | project = Just project
              }
            , emptyNotification
            , Cmd.none
            )

        _ ->
            ( model, emptyNotification, Cmd.none )


viewProject : Model -> Html ProjectMessage
viewProject model =
    let
        projectName =
            case model.project of
                Just project ->
                    project.name

                Nothing ->
                    "Erreur, projet invalide"
    in
    div []
        [ h1 [ class "is-size-3" ] [ text projectName ]
        ]
