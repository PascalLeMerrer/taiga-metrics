module Pages.ProjectList exposing (Model, ProjectListMessage(..), init, updateProjectList, viewProjectList)

import Data.Project exposing (Project)
import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData exposing (RemoteData, WebData)
import Requests.ProjectRequests exposing (listProjects)
import Views.Notifications exposing (..)


type ProjectListMessage
    = HandleProjectListResponse (WebData (List Project))


type alias Model =
    List Project


init : Model
init =
    []


updateProjectList : ProjectListMessage -> String -> Model -> ( Model, Notification, Cmd ProjectListMessage )
updateProjectList msg token model =
    case msg of
        HandleProjectListResponse RemoteData.NotAsked ->
            ( model
            , emptyNotification
            , Cmd.none
            )

        HandleProjectListResponse (RemoteData.Failure httpError) ->
            ( model
            , projectListFailedNotification
            , Cmd.none
            )

        HandleProjectListResponse (RemoteData.Success projects) ->
            ( projects
            , emptyNotification
            , Cmd.none
            )

        _ ->
            ( model, emptyNotification, Cmd.none )


viewProjectList : Model -> Html ProjectListMessage
viewProjectList model =
    div []
        [ h1 [ class "is-size-3" ] [ text "Vos Projets" ]
        , p [] [ em [] [ text "Vous devez être propriétaire d'un projet pour en configurer les métriques" ] ]
        , br [] []
        , ul []
            (List.map viewProject model)
        ]


viewProject : Project -> Html ProjectListMessage
viewProject project =
    li []
        [ a [ href <| "/projects/" ++ String.fromInt project.id ] [ text project.name ]
        ]
