module Routing exposing (Route(..), routeParser, toRoute, pushUrl)

import Browser.Navigation as Nav
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, int, map, oneOf, parse, s, top)


type Route
    = HomeRoute
    | ProjectsRoute
    | ProjectRoute Int
    | NotFoundRoute


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map HomeRoute top
        , map ProjectRoute (s "projects" </> int)
        , map ProjectsRoute (s "projects")
        ]


toRoute : Url -> Route
toRoute url =
    Maybe.withDefault NotFoundRoute (parse routeParser url)


pushUrl : Maybe Nav.Key -> String -> Cmd msg
pushUrl key hash =
    case key of
        Just aKey ->
            Nav.pushUrl aKey hash

        Nothing ->
            Cmd.none
