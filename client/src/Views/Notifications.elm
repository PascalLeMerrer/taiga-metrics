module Views.Notifications exposing (Notification, NotificationType(..), authenticationFailedNotification, deconnectionErrorNotification, emptyNotification, projectFailedNotification, projectListFailedNotification, serverErrorNotification)


type alias Notification =
    { body : String
    , title : String
    , messageType : NotificationType
    }


type NotificationType
    = SuccessNotification
    | ErrorNotification
    | NoNotification


emptyNotification : Notification
emptyNotification =
    { title = ""
    , body = ""
    , messageType = NoNotification
    }


serverErrorNotification : Notification
serverErrorNotification =
    { title = "Une erreur est survenue"
    , body =
        """Une erreur est survenue. Veuillez vérifier votre connexion avant de tenter à nouveau.
            Si l'erreur persiste, veuillez utiliser le formulaire de contact du site pour nous le signaler."""
    , messageType = ErrorNotification
    }


projectListFailedNotification : Notification
projectListFailedNotification =
    { title = "La requête a échoué"
    , body = """Une erreur est survenue lors de la récupération de la liste des projets.
                 Vous pouvez essayer de vous reconnecter avant de faire une autre tentative."""
    , messageType = ErrorNotification
    }


projectFailedNotification : Notification
projectFailedNotification =
    { title = "La requête a échoué"
    , body = """Une erreur est survenue lors de la récupération des caractéristiques du projet.
                 Vous pouvez essayer de vous reconnecter avant de faire une autre tentative."""
    , messageType = ErrorNotification
    }


authenticationFailedNotification : Notification
authenticationFailedNotification =
    { title = "La connexion a échoué"
    , body = "Cette combinaison nom d'utilisateur / mot de passe est erronée."
    , messageType = ErrorNotification
    }


deconnectionErrorNotification : Notification
deconnectionErrorNotification =
    { title = "La déconnexion a échoué"
    , body = ""
    , messageType = ErrorNotification
    }
