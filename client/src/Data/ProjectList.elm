module Data.ProjectList exposing (projectListDecoder)

import Data.Project exposing (Project, projectDecoder)
import Json.Decode exposing (Decoder, bool, decodeString, list)


projectListDecoder : Decoder (List Project)
projectListDecoder =
    Json.Decode.list projectDecoder
