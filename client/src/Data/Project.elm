module Data.Project exposing (Project, projectDecoder)

import Data.UserStoryStatus exposing (..)
import Json.Decode as Decode exposing (Decoder, int, list, string)
import Json.Decode.Pipeline exposing (optional, required)


type alias Project =
    { name : String
    , id : Int
    , userId : Int
    , statuses : List UserStoryStatus
    }


projectDecoder : Decoder Project
projectDecoder =
    Decode.succeed Project
        -- fields MUST be in the same order than in the JSON response
        |> required "name" string
        |> required "id" int
        |> required "user_id" int
        |> optional "us_statuses" (list statusDecoder) []
