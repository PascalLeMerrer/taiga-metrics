module Data.UserStoryStatus exposing (UserStoryStatus, statusDecoder)

import Json.Decode as Decode exposing (Decoder, int, string)
import Json.Decode.Pipeline exposing (required)


type alias UserStoryStatus =
    { id : Int
    , name : String
    , order : Int
    }


statusDecoder : Decoder UserStoryStatus
statusDecoder =
    Decode.succeed UserStoryStatus
        -- fields MUST be in the same order than in the JSON response
        |> required "id" int
        |> required "name" string
        |> required "order" int
