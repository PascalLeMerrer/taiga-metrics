module Data.Connection exposing (Connection)

import Data.User exposing (User)
import RemoteData exposing (WebData)


type alias Connection =
    { authenticated : Bool
    , isPasswordVisible : Bool
    , password : String
    , token : String
    , userStatus : WebData User
    , email : String
    }
