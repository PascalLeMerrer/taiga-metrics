module Data.User exposing (User, userDecoder)

import Json.Decode as Decode exposing (Decoder, string)
import Json.Decode.Pipeline exposing (required)


type alias User =
    { auth_token : String
    , full_display_name : String
    , username : String
    }


userDecoder : Decoder User
userDecoder =
    Decode.succeed User
        -- fields MUST be in the same order than in the JSON response
        |> required "auth_token" string
        |> required "full_name_display" string
        |> required "email" string
